## Simple hill model.

Here we give an example hill muscle model. 
It is meant to be a simple way of learning the steps of Zajac/van Soest.

The first file to checkout is go_simple_hill_model, which just starts this model out in equilibrium with gravity.
1. you can run it directly
2. inspect the ode function to see how it runs. 
3. you can imagine writing a tiny wrapper-function to enclose this ode.
---

