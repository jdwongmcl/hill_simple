function stated = ode_simple_hill_type(t,state,stim)
% function out = ode_simple_hill_type(t,state,parms)
% meant to be a didactic tool for teaching the hill-model =)
% 

%states: the joint angle+vel, the length of the muscle (norm to 1=opt), and
%the activation level of the muscle 'alpha'.
phi = state(1);     % angle of joint
phi_dot = state(2); % angular velocity
l_ce_norm = state(3);    % length of the muscle [0,1]
alpha = state(4);   % activation of the muscle [0,1]

% we need to compute state derivatives for 2:4!
% Broad idea for hill-type derivative calculation: get ddt:muscle length! 
% Begin with force balance at musculo-tendon junction.
% sum(F) = ma ; 
% sum(F) >> ma; 
% so simplify sum(F) = 0 %this is mostly Knoek/Zajac contribution I think.
% then:
% sum(F) = F_tendon + F_contractile
% F_tendon = k_tendon*L_tendon                  (assuming linear here)
% F_contractile = F_CSA*alpha*FL(l_ce)*FV(v). 
% and since F_contractile = F_tendon
% F_tendon/F_CSA/alpha/F_FL(l_ce) = F_FV(v)
% and since we know all LHS, we can solve for v by doing FV^{-1}

% First: compute tendon force which will do two things:
% 1: give us acceleration (second last line of code)
% 2: give us the total product of F_CSA*alpha*F_FL(l_ce)*F_FV(v)
% to get L_tendon assume some first-order relation between angle and
% muscle-tendon-and-contractile-length, and subtract contractile length
loi_offset = 0.1;       %loi = Length Origin to Insertion
moment_arm = 0.03;      %assume constant
loi = loi_offset + moment_arm * phi; 
l_ce_opt = 0.04;
l_tendon = loi - l_ce_norm*l_ce_opt;
kse = 6.885283370541919e3;%n/m. this is quite soft 

l_tendon_slack = 0.1;               %cm slack length   
F_tendon = kse * (l_tendon-l_tendon_slack);

% compute force contractile element

% F_contractile = F_CSA*alpha*FL*FV. 
% compute FL effect:
c =[-6.25,12.5,-5.25]; %parabola where f(1)=1;f(1+/-0.4) = 0
fun_fl =@(x) c(1)*x.^2 + c(2)*x + c(3);
F_fl = fun_fl(l_ce_norm);
F_fl = max(F_fl,0);                 %catch negative values.

F_CSA = 4905; %maximum isometric force of this muscle. strong bicep =)
F_FV = F_tendon/F_fl/alpha/F_CSA;

% invert a linear force-velocity relation
m = -1/13;b = 1;
fun_inv_fv = @(force) (force - b)/m;
v_ce = fun_inv_fv(F_FV);
v_ce = max(v_ce,0);                 %catch negative values.

% compute what the change in activation, alpha_dot, is. 
tau = 1e-1;
alpha_dot = 1/tau*(stim - alpha);

% compute change in acceleration. here we're just moving a limb. 
I = .1;
M = 1;
m_joint_centroid = 0.15;
g = 9.81;
moment_arm_g = sin(phi)*m_joint_centroid;
g_torque = moment_arm_g * M * g;
phi_dot_dot = (F_tendon * moment_arm - g_torque)/I;
% return the state derivatives
stated=[phi_dot; phi_dot_dot; v_ce; alpha_dot];